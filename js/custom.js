$(function() {

	$('#chart li').each(function(){

		var pc = $(this).attr('title');
		pc = pc > 100 ? 100 : pc;
		$(this).children('.percent').html(pc+'%');
		var ww = $(this).width();
		var len = parseInt(ww, 10) * parseInt(pc, 10) / 100;
		$(this).children('.bar').animate({ 'width' : len + 'px'},1500);

	});

});

/*$(function(){

	var vierkantE1 = $('section.vierkant'),
		vierkantE1Offset = vierkantE1.offset().top/2,
		documentEl = $(document);

		documentEl.on('scroll', function() {
			if (documentEl.scrollTop() > vierkantE1Offset && vierkantE1.hasClass('hidden') ) vierkantE1.removeClass('hidden');
		});

	
})*/



$(document).ready(function() {

		var windowHeight = $(window).height();
		var windowScrollPosTop = $(window).scrollTop();
		var windowScrollPosBottom = windowHeight + windowScrollPosTop;

	$.fn.revealOnScroll = function(direction) {

		return this.each(function(){

			var objectOffset = $(this).offset();
			var objectOffsetTop = objectOffset.top;

			if (!$(this).hasClass("hidden")) {

				if (direction == "right") {

					$(this).css ({
						"opacity" : 0,
						"right"	  : "200px"
					})

				} else {
					$(this).css ({
						"opacity" : 0,
						"right"	  : "-200px"
					})

				}

				$(this).addClass("hidden");

			}
		

		if (!$(this).hasClass("animation-complete")) {
			if (windowScrollPosBottom > objectOffsetTop) {
			$(this).animate({"opacity" : 1, "right" : 0}, 1000).addClass("animation-complete");
		}


		}

		});
	}

	$(window).scroll(function(){

		windowHeight = $(window).height();
		windowScrollPosTop = $(window).scrollTop();
		windowScrollPosBottom = windowHeight + windowScrollPosTop;
	
		 $(".verhaal-2,.verhaal-3").revealOnScroll();
		 $(".profileFoto, .picture").revealOnScroll("right");
		
	});
});